# Utiliser composer dans vos projets web
Ce dépôt de test sert d'exemple à une série d'articles disponibles sur mon blog à l'adresse [https://adrien-ruiz.fr/](https://adrien-ruiz.fr/).

## Pré-requis

 - php: >=5.3.2

## Installation

Se placer dans le répertoire destiné à accueillir les sources de ce dépôt, et lancer les commandes ci-dessous:

```
> git clone git@gitlab.com:ruiadr/test.git .
> composer install
```